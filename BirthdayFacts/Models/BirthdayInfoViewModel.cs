﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BirthdayFacts.Models
{
    public class BirthdayInfoViewModel
    {
        [Required]
        public DateTime? Birthday { get; set; }

        public bool Completed { get; set; }

        public int CurrentAge { get; set; }

        public int DaysOld { get; set; }

        public int DaysUntilNextBirthday { get; set; }

        public long DaysUntil100 { get; set; }

        public List<DayTotal> BirthdayDayOfWeekPercentages { get; set; }

        public List<DayTotal> BirthdayDayOfWeekPercentages100thBirthday { get; set; }

    }


    public class DayTotal
    {
        public DayOfWeek DayOfWeek { get; set; }

        public decimal Total { get; set; }
    }
}