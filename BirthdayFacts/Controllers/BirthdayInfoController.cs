﻿using BirthdayFacts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BirthdayFacts.Controllers
{
    public class BirthdayInfoController : Controller
    {
        //
        // GET: /BirthdayInfo/

        public ActionResult Index()
        {
            return View(new BirthdayInfoViewModel());
        }

        //
        // POST: /BirthdayInfo/
        [HttpPost]
        public ActionResult Index(BirthdayInfoViewModel model)
        {
            if (ModelState.IsValid)
            {
                var birthday = model.Birthday.Value;

                var ageSpan = DateTime.Now.Subtract(birthday);
                model.DaysOld = ageSpan.Days;
                model.CurrentAge = ageSpan.Days / 365;


                var birthday100YearsOld = birthday.AddYears(100);
                var nextBirthDay = new DateTime(DateTime.Now.Year, birthday.Month, GetNextYearsDay(birthday));
                // If this years birthday has already past, add a year
                if (nextBirthDay <= DateTime.Now)
                {
                    nextBirthDay = nextBirthDay.AddYears(1); 
                }

                var daysUntil100 = 0;
                var lastCheckedDay = birthday100YearsOld;

                while (lastCheckedDay > DateTime.Now)
                {
                    daysUntil100++;
                    lastCheckedDay = lastCheckedDay.AddDays(-1) ; 
                }

                model.DaysUntil100 = daysUntil100;
                model.DaysUntilNextBirthday = nextBirthDay.Subtract(DateTime.Now).Days;

                model.BirthdayDayOfWeekPercentages = CalculateDayTotalsForBirthday(birthday, DateTime.Now.Year);
                model.BirthdayDayOfWeekPercentages100thBirthday = CalculateDayTotalsForBirthday(birthday, birthday100YearsOld.Year); 

                model.Completed = true;
            }

            return View(model);
        }

        private int GetNextYearsDay(DateTime birthday)
        {
            // Check for leap day, if so, use 2/28 as the day for next year
            var birthDayDay = birthday.Month == 2 && birthday.Day == 29 ? 28 : birthday.Day;

            return birthDayDay; 
        }


        private List<DayTotal> CalculateDayTotalsForBirthday(DateTime birthDay, int endYear)
        {
            var birthDays = new List<DateTime>();

            for (var year = birthDay.Year; year <= endYear; year++)
            {
                birthDays.Add(new DateTime(year, birthDay.Month, GetNextYearsDay(birthDay)));
            }

            // Group the birth days by day of week
            var birthDayDayOfWeek = birthDays.GroupBy(d => d.DayOfWeek)
                .Select(g => new
                {
                    DayOfWeek = g.Key,
                    Total = g.Count()
                })
                .OrderBy(g => g.DayOfWeek)
                .Select(d => new DayTotal
                    {
                        DayOfWeek = d.DayOfWeek,
                        Total = (decimal)d.Total / (decimal)birthDays.Count()
                    })
                .ToList();

            return birthDayDayOfWeek; 

        }

    }
}
