﻿$(function () {
    var yearRange = "1900:" + (new Date().getFullYear() + 50);

    $('.datepicker').datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: yearRange
    });
});